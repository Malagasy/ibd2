exercice 5
127.0.0.1:6379> set user andy
OK
127.0.0.1:6379> get user
"andy"

exercice 6
127.0.0.1:6379> hset languages lang1 "PHP"
(integer) 1
127.0.0.1:6379> hset languages lang2 "JAVA"
(integer) 1
#ensemble des données
127.0.0.1:6379> hlen languages
(integer) 2
#données de la clé "languages"
127.0.0.1:6379> hkeys languages
1) "lang1"
2) "lang2"
127.0.0.1:6379> hset languages nombre 1
(integer) 1
127.0.0.1:6379> hincrby languages nombre 2
(integer) 3

exercice 7
127.0.0.1:6379> rpush abc a
(integer) 1
127.0.0.1:6379> rpush abc b
(integer) 2
127.0.0.1:6379> rpush abc c
(integer) 3
127.0.0.1:6379> lrange abc
(error) ERR wrong number of arguments for 'lrange' command
127.0.0.1:6379> lrange abc 0
(error) ERR wrong number of arguments for 'lrange' command
127.0.0.1:6379> lrange abc 0 n
(error) ERR value is not an integer or out of range
127.0.0.1:6379> lrange abc 0 2
1) "a"
2) "b"
3) "c"
127.0.0.1:6379> lrange abc 0 1
1) "a"
2) "b"
#ici à l'aide de "multi" et "queue" on peut travailler de manière atomique
127.0.0.1:6379> multi
OK
127.0.0.1:6379> rpush abc d
QUEUED
127.0.0.1:6379> lrange abc 0 3
QUEUED
127.0.0.1:6379> rpush abc e
QUEUED
127.0.0.1:6379> exec
1) (integer) 4
2) 1) "a"
   2) "b"
   3) "c"
   4) "d"
3) (integer) 5
#on efface le hash !
127.0.0.1:6379> expire languages 10
(integer) 1
127.0.0.1:6379> hvals languages
1) "PHP"
2) "JAVA"
3) "3"
127.0.0.1:6379> hvals languages
(empty list or set)

exercice 8
REDIS ne prend pas en compte les modifications car il n'a pas eu le temps de les faire avant le crash.
REDIS prend en compte le total de clef.

