Exercice 1 : Pourquoi ? Comment ? 
Que permet l’approche “NoSQL” par rapport à l’approche “SQL" ?
Quelles sont les limites de l’approche “SQL” ?
L'approche NoSQL permet de construire son modèle de données et de le faire évoluer facilement. Tandis que l'approche SQL le modèle de données est rigide et toutes modifications dans la structure ont un impact majeur.

Exercice 2 : CAP
Comment s’inscrit le théorème CAP dans l’approche SQL ?
Comment NoSQL s’inscrit dans le théorème CAP ?

CAP => Cohérence, Disponibilité, Tolérance
L'approche SQL répond au besoin Tolérance, Disponibilité
L'approche NoSQL répond au besoin Cohérence, Disponibilité

Exercice 3 : Types de bases NoSQL
Pour chacun des types de bases de données suivants, évoquer brièvement leurs avantages et inconvénients
Document Database => Permet de stocker des données dont la structure peut évoluer rapidement
	ADV 	=> structure de données souple
	DESADV 	=> Pas de schémas, risque de bordel
Key-Value Database => Orienté performance sur des jeux de données d'une structure simple
	ADV		=> 
Graph Database => Modèle de données orienté graphe qui permet de déterminer des relations entre les doucments de la base de données.
Wide column database => Adapté à des structures de données ayant un grand nombre de colonnes.

Exercice 4 : Pratiques de modélisation
Quel modèle(s) utiliser sur une base de données documents pour modéliser un chat type irc (utilisateurs, rooms, messages) pour les cas suivants:
Avec un grand nombre d’utilisateurs par room 
Avec un grand nombre de room mais peu d’utilisateurs par room
Pourquoi ?
PS: plusieurs réponses sont acceptables, la justification est importante :)

=> Avec un grand nombre d'utilisateurs par room 

room{
	nom,
	utilisateurs: [utilisateur,utilisateur]
}
message{
	auteur,
	message,
	room
}
On met les utilisateurs dans le document "room" afin de réquéter rapidement tous les utilisateurs de chaque room.

=> Avec un grand nombre de room mais peu d'utilisateurs par room

utilisateur{
	pseudo
	rooms: [{nom}, ... ]
}

message{
	auteur,
	message,
	room
}
On met les rooms dans le document "utilisateur" afin de réquéter rapidement toutes les rooms pour le peu d'utilisateur.

